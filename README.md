# OpenML dataset: KNYC-Metars-2016

https://www.openml.org/d/43725

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
METAR is a format for reporting weather information. A METAR weather report is predominantly used by pilots in fulfillment of a part of a pre-flight weather briefing, and by meteorologists, who use aggregated METAR information to assist in weather forecasting.
Content
This is the METARs aggregated information for 2016 in KNYC.
Acknowledgements
Thanks to wunderground for providing the data
Inspiration
This dataset is ment to be used as a extra information for those willing to extract conclusions from their own dataset where hourly the weather information could be useful for their predictions / analysis. You can contact me if you have any doubt or suggestion.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43725) of an [OpenML dataset](https://www.openml.org/d/43725). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43725/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43725/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43725/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

